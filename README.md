Very WIP guile binding to the C++ implementation of Cap'n Proto.

The goal is for this package to be able to integrate smoothly with
[goblins](https://gitlab.com/spritely/goblins), both as an alternative
to the nascent ocapn protocol and possibly later as the basis of a
bridge between the two protocols.
