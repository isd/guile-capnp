#include <libguile.h>
#include <capnp/schema.h>
#include <capnp/dynamic.h>

namespace guile_capnp {

using namespace capnp;

SCM scm_from_dyn_enum(DynamicEnum);
SCM scm_from_dyn_list(DynamicList::Reader);
SCM scm_from_dyn_struct(DynamicStruct::Reader);
SCM scm_from_dyn_value(DynamicValue::Reader);

SCM scm_from_dyn_value(DynamicValue::Reader value) {
  // Convert a dynamic value to a scheme value. Mappings:
  //
  // - A value of type Void returns ()
  // - booleans return scheme booleans
  // - Integer types return scheme integers
  // - Floating point values return scheme floats
  // - Text returns a string
  // - List returns a scheme list, converting the elements recursively.
  // - enums return the enumerant names as symbols, or the numeric value
  //   if the name is unknown.
  // - structs return alists with field names (encoded as symbols) as keys.
  switch (value.getType()) {
    case DynamicValue::VOID:
      return SCM_EOL;
    case DynamicValue::BOOL:
      if (value.as<bool>()) {
        return SCM_BOOL_T;
      } else {
        return SCM_BOOL_F;
      }
    case DynamicValue::INT:
      return scm_from_signed_integer(value.as<int64_t>());
    case DynamicValue::UINT:
      return scm_from_unsigned_integer(value.as<uint64_t>());
    case DynamicValue::FLOAT:
      return scm_from_double(value.as<double>());
    case DynamicValue::TEXT:
      return scm_from_utf8_string(value.as<Text>().cStr());
    case DynamicValue::LIST:
      return scm_from_dyn_list(value.as<DynamicList>());
    case DynamicValue::ENUM:
      return scm_from_dyn_enum(value.as<DynamicEnum>());
    case DynamicValue::STRUCT:
      return scm_from_dyn_struct(value.as<DynamicStruct>());
      // TODO: DATA
      // TODO: ANY_POINTER
      // TODO: CAPABILITY
    default:
      KJ_FAIL_ASSERT("TODO: other types");
  }
}

SCM scm_from_dyn_list(DynamicList::Reader list) {
  // Convert a dynamic list to a scheme list.

  SCM ret = SCM_EOL;
  // Build up the cons list starting from the end:
  uint i = list.size();
  while(i > 0) {
    i--;
    SCM element = scm_from_dyn_value(list[i]);
    ret = scm_cons(element, ret);
  }
  return ret;
}

SCM scm_from_dyn_enum(DynamicEnum enumValue) {
  // Convert a dynamic enum to a scheme value. Return the name of
  // the enumerant as a symbol, or the raw numeric value if the
  // enumerant is not recognized.
  KJ_IF_MAYBE(enumerant, enumValue.getEnumerant()) {
    return scm_from_utf8_symbol(enumerant->getProto().getName().cStr());
  } else {
    return scm_from_unsigned_integer(enumValue.getRaw());
  }
}

SCM scm_from_dyn_struct(DynamicStruct::Reader structValue) {
  // Convert a dynamic struct into a scheme value. We encode
  // structs as alists with the field names as symbols.
  SCM ret = SCM_EOL;
  for(auto field: structValue.getSchema().getFields()) {
    if(!structValue.has(field)) {
      continue;
    }
    SCM key = scm_from_utf8_symbol(field.getProto().getName().cStr());
    SCM value = scm_from_dyn_value(structValue.get(field));
    ret = scm_cons(scm_cons(key, value), ret);
  }
  return ret;
}

void init_extension() {
}

};

extern "C"
void init_guile_capnp(void) noexcept {
  guile_capnp::init_extension();
}
