
cxx_src := $(wildcard *.c++)
objects := $(cxx_src:.c++=.o)

CXXFLAGS2 := $(CXXFLAGS) `pkg-config --cflags guile-3.0 capnp` -fPIC

all: libguile-capnp.so

libguile-capnp.so: $(objects)
	$(CXX) $(CXXFLAGS2) -shared -o $@ $(objects)

%.o: %.c++
	$(CXX) $(CXXFLAGS2) -c -o $@ $<

clean:
	rm -f *.o *.so

.PHONY: all clean
